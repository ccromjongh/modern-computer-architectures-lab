# %%
import os
import subprocess
import signal
import time
from IPython.display import Image
import re

base_dir = '/home/user/workspace/assignment1'


def set_work_dir(relative=''):
    new_dir = base_dir + relative
    os.chdir(new_dir)


# %%
# Testing code for subprocess

print('stuff')
out = subprocess.Popen(['ls', '-l', 'configurations/example-4-issue'],
                       stdout=subprocess.PIPE,
                       stderr=subprocess.STDOUT)
stdout, stderr = out.communicate()
stdout2 = str(stdout).replace('\\n', '\n')
print(stdout, '\n\n', stdout2, '\n\n', stderr, '\n\n', type(stdout))


# %%

def run_compiler(job='pocsag', flags='-O3'):
    set_work_dir('/configurations/example-4-issue')
    run_out = subprocess.Popen(['run', job, flags],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
    stdout, stderr = run_out.communicate()
    stdout = str(stdout).split('\\n')
    stderr = str(stderr).split('\\n')
    set_work_dir()
    return stdout, stderr


# %%

def create_config(issue_width=4, mem_load=1, mem_store=1, mem_pft=1,
                  alu0=4, mpy0=2, memory0=1, r0=64, w0=8):
    # issue_width = 4  # At least 2
    # mem_load = 1
    # mem_store = 1
    # mem_Pft = 1
    # alu0 = 4
    # mpy0 = 2
    # memory0 = 1
    # r0 = 64
    # w0 = 8

    config_template = """    
#-----------------------------------------------------------------------------#
#                            Global configuration                             #
#-----------------------------------------------------------------------------#

# Overall issue width. This is the number of syllables that can be executed
# per cycle, regardless of clustering. This must be at least 2.
RES: IssueWidth     {0}

# The following parameters specify the number of 32-bit connections to the data
# cache, for memory loads, stores, and prefetches respectively.
RES: MemLoad        {1}
RES: MemStore       {2}
RES: MemPft         {3}

# The following parameter specifies the number of clusters to use. It can be
# 1, 2 or 4.
# NOTE: do NOT uncomment this line. It is commented because the number of
# clusters is confusingly not actually part of the machine model, but
# a compiler flag. The compile script will search for this line and use it to
# set the -width compiler flag accordingly.
# We only work with a single cluster,so DO NOT CHANGE THIS PARAMETER.
# ***Clusters***    1

#-----------------------------------------------------------------------------#
#                           Cluster 0 configuration                           #
#-----------------------------------------------------------------------------#

# The following parameter specifies the maximum number of syllables that can be
# decoded by this cluster per cycle. This must be equal to the value of
# RES:IssueWidht in global configuration.
RES: IssueWidth.0   {0}

# The following parameter specifies the number of ALU syllables that can be
# executed by this cluster per cycle.
RES: Alu.0          {4}

# The following parameter specifies the number of multiply syllables that can
# be executed by this cluster per cycle.
RES: Mpy.0          {5}

# The following parameter specifies the number of memory syllables that can be
# executed by this cluster per cycle.
RES: Memory.0       {6}

# The following two parameters specify the number of inter-cluster
# communication syllables that can be executed by this cluster per cycle. A
# register move from one cluster to another consists of a SEND syllable in the
# source cluster and a RECV syllable in the destination cluster. The number of
# SEND instructions per cycle in this cluster are governed by CopySrc, while
# RECV is governed by CopyDst.
# We only work with a single cluster,so DO NOT CHANGE THIS PARAMETER.
RES: CopySrc.0      {7}
RES: CopyDst.0      {8}

# The following parameter specifies the number of 32-bit general purpose
# registers available to this cluster.
REG: $r0            64

# The following parameter specifies the number of single bit condition
# registers available to this cluster.
REG: $b0            8

#=============================================================================#
#            For the assignments, don't change anything below here            #
#=============================================================================#

# Functional unit latencies for cluster 0.
DEL: AluR.0         0
DEL: Alu.0          0
DEL: CmpBr.0        0
DEL: CmpGr.0        0
DEL: Select.0       0
DEL: Multiply.0     1
DEL: Load.0         1
DEL: LoadLr.0       1
DEL: Store.0        0
DEL: Pft.0          0
DEL: CpGrBr.0       0
DEL: CpBrGr.0       0
DEL: CpGrLr.0       0
DEL: CpLrGr.0       0
DEL: Spill.0        0
DEL: Restore.0      1
DEL: RestoreLr.0    1

CFG: Quit           0
CFG: Warn           0
CFG: Debug          0"""

    config_string = config_template.format(issue_width, mem_load, mem_store,
                                           mem_pft, alu0, mpy0, memory0, r0, w0)

    return config_string


def write_config(config_string):
    set_work_dir('/configurations/example-4-issue')
    f = open('configuration.mm', 'w')
    f.write(config_string)
    f.close()
    set_work_dir()


# %%

def get_image(job, dpi=100, timeout=0.3):
    set_work_dir('/configurations/example-4-issue/output-{}.c'.format(job))

    rgg_out = subprocess.Popen(['rgg', 'a.out', '-g', 'gmon-nocache.out'],
                               preexec_fn=os.setsid)
    # Small time-out to generate file.
    time.sleep(timeout)

    # Run command to create graph image
    os.system('echo gcall.vcg | vcg -ppmoutput gcall.ppm -xdpi {0} -ydpi {0}'.format(dpi))
    # Kill rgg process by sending the SIGTERM signal to all the process groups
    os.killpg(os.getpgid(rgg_out.pid), signal.SIGTERM)

    # Convert image to png and remove ppm file
    os.system('pnmtopng gcall.ppm > gcall.png')
    os.system('rm gcall.ppm')
    set_work_dir()


def disp_image(job):
    return Image(filename='configurations/example-4-issue/output-{}.c/gcall.png'.format(job))


def get_gmon(job):
    set_work_dir('/configurations/example-4-issue/output-{}.c'.format(job))
    # Generate gmon text file
    os.system('gprof a.out gmon-nocache.out > gmon-nocache.txt')
    f = open('gmon-nocache.txt', 'r')
    f1 = f.readlines()
    functions = []

    for i in range(5, len(f1)):
        line = f1[i]
        search = re.search('([0-9.]+) +([0-9.]+) +([0-9.]+) +([0-9.]+) +([0-9.]+) +([0-9.]+) +(\w+)', line)
        if not search:
            break
        result = {'time': float(search.group(1)), 'cumulative': float(search.group(2)), 'self': float(search.group(3)),
                  'calls': int(search.group(4)), 'self_ms': float(search.group(5)), 'total_ms': float(search.group(6)),
                  'name': search.group(7)}
        functions.append(result)

    set_work_dir()
    return functions


def get_prof_file(job, type='static', filter=True):
    set_work_dir('/configurations/example-4-issue/output-{}.c'.format(job))
    f = open('{}_prof_file.txt'.format(type), 'r')
    f1 = f.readlines()
    functions = []

    for line in f1:
        search = re.search('operation:[ \t]+(\w+)[ \t]+n\. times:[ \t]+([0-9]+)', line)
        if not search:
            break
        operation = search.group(1)
        n_times = int(search.group(2))
        if filter and n_times == 0:
            continue
        result = {'operation': operation, 'n_times': n_times}
        functions.append(result)

    set_work_dir()
    return functions

def get_exec_cycles(job):
    set_work_dir('/configurations/example-4-issue/output-{}.c'.format(job))
    f = open('ta.log.000', 'r')
    f.readline()  # Skip line
    line = f.readline()
    search = re.search('([0-9]+)', line)
    if not search:
        return None

    cycles = int(search.group(1))

    set_work_dir()
    return cycles


#%%

config_string = create_config(issue_width=2)
write_config(config_string)

job = 'pocsag'
output, error = run_compiler(job=job)
print('\n'.join(output), '\nErrors:', '\n'.join(error))

print('\n\n-------------------------------------\nExecution cycles:', get_exec_cycles(job))

get_image(job, 100)
disp_image(job)

#%%

get_gmon(job)
# get_prof_file(job, 'static')
# get_prof_file(job, 'dyn')
